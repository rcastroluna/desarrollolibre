package cl.desarrollolibre.checkin.modelos;

/**
 * Created by RafaelCastro on 18/9/15.
 */
public class Ticket {
    private String identificador,matricula;
    private String horaEntrada,horaSalida;

    public Ticket(String matricula,String identificador, String horaEntrada, String horaSalida) {
        this.identificador = identificador;
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
        this.matricula = matricula;
    }


    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }
}

