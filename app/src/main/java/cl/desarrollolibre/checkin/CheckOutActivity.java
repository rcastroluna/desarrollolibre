package cl.desarrollolibre.checkin;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import cl.desarrollolibre.checkin.db.DBHelper;

/**
 * Created by RafaelCastro on 22/9/15.
 */
public class CheckOutActivity extends AppCompatActivity{
    TextView txtCodigoTicket;
    Button btnCheckOut;
    RelativeLayout rlCheckIn,rlCheckOut;
    Toolbar toolbar;
    private static String TAG="CheckOutActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        Bundle extras = getIntent().getExtras();
        txtCodigoTicket = (TextView)findViewById(R.id.txtCodigoTicket);

        rlCheckIn = (RelativeLayout)findViewById(R.id.rlCheckIn);
        rlCheckOut = (RelativeLayout)findViewById(R.id.rlCheckOut);

        btnCheckOut = (Button)findViewById(R.id.btnCheckOut);
        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigoTicket = txtCodigoTicket.getText().toString();
                String horaSalida = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                //Revisar que el ticket esté vigente
                Log.d("EXISTE", String.valueOf(existeTicket(codigoTicket)));

                if (existeTicket(codigoTicket) == 1) {
                    sacarVehiculo(codigoTicket, horaSalida, 0);

                    Intent intent = new Intent(CheckOutActivity.this, SalidaActivity.class);
                    intent.putExtra("identificador", codigoTicket);
                    startActivity(intent);


                } else {
                    Snackbar.make(v, "Este ticket no existe", Snackbar.LENGTH_LONG).show();
                }
            }
        });


        rlCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckOutActivity.this, CheckInActivity.class);
                intent.putExtra("tabPosition", 0);
                startActivity(intent);
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        txtCodigoTicket.setText("");
    }


    public int existeTicket(String identificacion)
    {
        int existe=-1;
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql= "SELECT count(*) FROM automoviles WHERE identificador='"+identificacion+"' and estado=1";
        Cursor c = db.rawQuery(sql,null);
        c.moveToFirst();
        if (c.moveToFirst())
        {
            existe = c.getInt(0);
        }
        c.close();
        db.close();
        return existe;
    }






    public int sacarVehiculo(String identificacion, String fechaSalida,int estado)
    {
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int filas=-1;
        String sql="UPDATE automoviles SET estado="+estado+",fechaSalida='"+fechaSalida+"'"+
                " WHERE identificador='"+identificacion+"' and estado=1";
        Log.d(TAG, sql);
        try{
            db.execSQL(sql);
            filas=1;
            Log.d("SAVE", String.valueOf(filas));
        }catch (Exception e){
            filas=-2;
            Log.d("SAVE", e.getMessage());
        }
        db.close();
        return filas;

    }
}
