package cl.desarrollolibre.checkin;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cl.desarrollolibre.checkin.db.DBHelper;
import cl.desarrollolibre.checkin.modelos.Ticket;

/**
 * Created by RafaelCastro on 18/9/15.
 */
public class SalidaActivity extends AppCompatActivity {
    private Toolbar toolbar;
    TextView lblIngreso,lblSal,lblIdentificador,txtTiempo,txtMonto,lblMatricula;
    WindowManager manager;
    RelativeLayout rlCheckIn,rlCheckOut;
    String identificador;
    ArrayList<Ticket> tickets = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salida);
        Bundle extras = getIntent().getExtras();
        identificador = extras.getString("identificador");
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        lblMatricula = (TextView)findViewById(R.id.lblMatricula);
        lblIngreso = (TextView)findViewById(R.id.lblIngreso);
        lblSal = (TextView)findViewById(R.id.lblSal);
        lblIdentificador = (TextView)findViewById(R.id.lblIdentificador);
        txtTiempo = (TextView)findViewById(R.id.txtTiempo);
        txtMonto = (TextView)findViewById(R.id.txtMonto);
        rlCheckIn = (RelativeLayout)findViewById(R.id.rlCheckIn);
        rlCheckOut = (RelativeLayout)findViewById(R.id.rlCheckOut);

        getDatosTicket(identificador);
        lblIdentificador.setText(identificador);
        lblMatricula.setText(tickets.get(0).getMatricula());
        lblIngreso.setText(tickets.get(0).getHoraEntrada());
        lblSal.setText(tickets.get(0).getHoraSalida());
        txtTiempo.setText(String.valueOf(diferenciaMinutos(tickets.get(0).getHoraEntrada(), tickets.get(0).getHoraSalida())));
        txtMonto.setText(String.valueOf(calculaMontos(diferenciaMinutos(tickets.get(0).getHoraEntrada(), tickets.get(0).getHoraSalida()))));

        rlCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalidaActivity.this, CheckInActivity.class);
                intent.putExtra("tabPosition", 0);
                startActivity(intent);
            }
        });

        rlCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalidaActivity.this, CheckOutActivity.class);
                intent.putExtra("tabPosition", 1);
                startActivity(intent);
            }
        });


    }



    public long diferenciaMinutos(String fechaEntrada, String fechaSalida)
    {
        long minutos = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date dtFechaEntrada = new Date();
        Date dtFechaSalida = new Date();
        try {
            dtFechaEntrada = sdf.parse(fechaEntrada);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            dtFechaSalida = sdf.parse(fechaSalida);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();cal1.setTime(dtFechaEntrada);
        Calendar cal2 = Calendar.getInstance();cal2.setTime(dtFechaSalida);

        long diff = cal2.getTimeInMillis() - cal1.getTimeInMillis();
        minutos = diff/(1000*60);
        return minutos;
    }



    public ArrayList<Ticket> getDatosTicket(String identificacion)
    {
        int existe=-1;
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql= "SELECT matricula," +
                "identificador," +
                "fechaIngreso," +
                "fechaSalida " +
                "FROM automoviles WHERE identificador='"+identificacion+"'";

        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        if (c.moveToFirst())
        {
          tickets.add(new Ticket(c.getString(0),c.getString(1),c.getString(2),c.getString(3)));
        }
        c.close();
        db.close();
        return tickets;
    }



    public String calculaMontos(long tiempo)
    {
        String monto="0$";
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String sql= "SELECT valorPesos FROM tarifa " +
                "where ("+tiempo+"-minimo)>=0 " +
                "and (maximo-"+tiempo+")>=0";

        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        if (c.moveToFirst())
        {
            monto = c.getString(0);
        }
        return monto;
    }


}
