package cl.desarrollolibre.checkin;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bixolon.printer.BixolonPrinter;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cl.desarrollolibre.checkin.db.DBHelper;

public class CheckInActivity extends AppCompatActivity {
    static BixolonPrinter mBixolonPrinter;
TextView txtPrimDigitos,txtSegDigitos,txtTercDigitos,txtEstacionamiento;
Button btnCheckIn,btnPrint;
    RelativeLayout rlCheckOut;
    private Toolbar toolbar;
    String placa,TAG="MainActivity",estacionamiento;
    String horaFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
    private String mConnectedDeviceName = null;
    static final String ACTION_GET_DEFINEED_NV_IMAGE_KEY_CODES = "com.bixolon.anction.GET_DEFINED_NV_IMAGE_KEY_CODES";
    static final String ACTION_COMPLETE_PROCESS_BITMAP = "com.bixolon.anction.COMPLETE_PROCESS_BITMAP";
    static final String ACTION_GET_MSR_TRACK_DATA = "com.bixolon.anction.GET_MSR_TRACK_DATA";
    static final String EXTRA_NAME_NV_KEY_CODES = "NvKeyCodes";
    static final String EXTRA_NAME_MSR_MODE = "MsrMode";
    static final String EXTRA_NAME_MSR_TRACK_DATA = "MsrTrackData";
    static final String EXTRA_NAME_BITMAP_WIDTH = "BitmapWidth";
    static final String EXTRA_NAME_BITMAP_HEIGHT = "BitmapHeight";
    static final String EXTRA_NAME_BITMAP_PIXELS = "BitmapPixels";
    static final int REQUEST_CODE_SELECT_FIRMWARE = Integer.MAX_VALUE;
    static final int RESULT_CODE_SELECT_FIRMWARE = Integer.MAX_VALUE - 1;
    static final int MESSAGE_START_WORK = Integer.MAX_VALUE - 2;
    static final int MESSAGE_END_WORK = Integer.MAX_VALUE - 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        createDatabase();
        txtPrimDigitos = (TextView)findViewById(R.id.txtPrimDigitos);
        txtSegDigitos = (TextView)findViewById(R.id.txtSegDigitos);
        txtTercDigitos = (TextView)findViewById(R.id.txtTercDigitos);
        txtEstacionamiento = (TextView)findViewById(R.id.txtEstacAsignado);
        btnCheckIn = (Button)findViewById(R.id.btnCheckIn);
        btnPrint = (Button)findViewById(R.id.btnPrint);
        rlCheckOut = (RelativeLayout)findViewById(R.id.rlCheckOut);
        rlCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckInActivity.this, CheckOutActivity.class);
                startActivity(intent);
            }
        });


       txtPrimDigitos.addTextChangedListener(new TextWatcher() {

           public void onTextChanged(CharSequence s, int start,int before, int count)
           {
               // TODO Auto-generated method stub
               if(txtPrimDigitos.getText().toString().length()==2)
               {
                   txtSegDigitos.requestFocus();
               }
           }
           public void beforeTextChanged(CharSequence s, int start,
                                         int count, int after) {
               // TODO Auto-generated method stub

           }

           public void afterTextChanged(Editable s) {
               // TODO Auto-generated method stub
           }

       });


        txtSegDigitos.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(txtSegDigitos.getText().toString().length()==3)
                {
                    txtTercDigitos.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });


        txtTercDigitos.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (txtTercDigitos.getText().toString().length() == 2) {
                    txtEstacionamiento.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        mBixolonPrinter = new BixolonPrinter(this, mHandler, null);



        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                estacionamiento = txtEstacionamiento.getText().toString();
                if (validarPlaca(placa) && estacionamiento.length()>0)
                {

                    Log.d(TAG,"Imprimiendo");
                    mBixolonPrinter.printText(horaFecha,BixolonPrinter.ALIGNMENT_LEFT,1,20,false);
                    mBixolonPrinter.printText(placa,BixolonPrinter.ALIGNMENT_RIGHT,1,20,false);
                    mBixolonPrinter.print1dBarcode(placa,
                            BixolonPrinter.BAR_CODE_CODE128,
                            BixolonPrinter.ALIGNMENT_CENTER,
                            5,
                            160,
                            BixolonPrinter.HRI_CHARACTERS_BELOW_BAR_CODE,
                            false);
                }
            }
        });

        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placa = armarMatricula(txtPrimDigitos.getText().toString(),
                                       txtSegDigitos.getText().toString(),
                                       txtTercDigitos.getText().toString());
                Log.d(TAG,placa);
                estacionamiento = txtEstacionamiento.getText().toString();
                if (validarPlaca(placa) && estacionamiento.length()>0)
                {

                    String identificador = randomString(6);
                    ingresarVehiculo(identificador,placa,1,estacionamiento,horaFecha,"");
                    Intent intent = new Intent(CheckInActivity.this,ImpresoActivity.class);
                    intent.putExtra("identificador",identificador);
                    intent.putExtra("hora",horaFecha);
                    intent.putExtra("placa",placa);
                    startActivity(intent);

                }else{
                    Snackbar.make(v,"El formato de la patente no es válido",Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static Random rnd = new Random();

    String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public String armarMatricula(String s1, String s2, String s3)
    {
        if (s3.length()>0)
        {
            return s1+"-"+s2+"-"+s3;
        }else{
            return s1+"-"+s2;
        }
    }


    public static boolean validarPlaca(String placa) {
        boolean isValid = false;

        String expression = "^[a-zA-Z]{2}-[a-zA-Z0-9]{2}\\-?\\d{0,2}$";
        CharSequence inputStr = placa;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }



    private void dispatchMessage(Message msg) {
        switch (msg.arg1) {
            case BixolonPrinter.PROCESS_GET_STATUS:
                if (msg.arg2 == BixolonPrinter.STATUS_NORMAL) {
                    Toast.makeText(getApplicationContext(), "No error", Toast.LENGTH_SHORT).show();
                } else {
                    StringBuffer buffer = new StringBuffer();
                    if ((msg.arg2 & BixolonPrinter.STATUS_COVER_OPEN) == BixolonPrinter.STATUS_COVER_OPEN) {
                        buffer.append("Cover is open.\n");
                    }
                    if ((msg.arg2 & BixolonPrinter.STATUS_PAPER_NOT_PRESENT) == BixolonPrinter.STATUS_PAPER_NOT_PRESENT) {
                        buffer.append("Paper end sensor: paper not present.\n");
                    }

                    Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_SHORT).show();
                }
                break;

            case BixolonPrinter.PROCESS_GET_PRINTER_ID:
                Bundle data = msg.getData();
                Toast.makeText(getApplicationContext(), data.getString(BixolonPrinter.KEY_STRING_PRINTER_ID), Toast.LENGTH_SHORT).show();
                break;

            case BixolonPrinter.PROCESS_GET_BS_CODE_PAGE:
                data = msg.getData();
                Toast.makeText(getApplicationContext(), data.getString(BixolonPrinter.KEY_STRING_CODE_PAGE), Toast.LENGTH_SHORT).show();
                break;

            case BixolonPrinter.PROCESS_GET_PRINT_SPEED:
                switch (msg.arg2) {
                    case BixolonPrinter.PRINT_SPEED_LOW:
                        Toast.makeText(getApplicationContext(), "Print speed: low", Toast.LENGTH_SHORT).show();
                        break;
                    case BixolonPrinter.PRINT_SPEED_MEDIUM:
                        Toast.makeText(getApplicationContext(), "Print speed: medium", Toast.LENGTH_SHORT).show();
                        break;
                    case BixolonPrinter.PRINT_SPEED_HIGH:
                        Toast.makeText(getApplicationContext(), "Print speed: high", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;

            case BixolonPrinter.PROCESS_GET_PRINT_DENSITY:
                switch (msg.arg2) {
                    case BixolonPrinter.PRINT_DENSITY_LIGHT:
                        Toast.makeText(getApplicationContext(), "Print density: light", Toast.LENGTH_SHORT).show();
                        break;
                    case BixolonPrinter.PRINT_DENSITY_DEFAULT:
                        Toast.makeText(getApplicationContext(), "Print density: default", Toast.LENGTH_SHORT).show();
                        break;
                    case BixolonPrinter.PRINT_DENSITY_DARK:
                        Toast.makeText(getApplicationContext(), "Print density: dark", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;

            case BixolonPrinter.PROCESS_GET_POWER_SAVING_MODE:
                String text = "Power saving mode: ";
                if (msg.arg2 == 0) {
                    text += false;
                } else {
                    text += true + "\n(Power saving time: " + msg.arg2 + ")";
                }
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                break;

            case BixolonPrinter.PROCESS_AUTO_STATUS_BACK:
                StringBuffer buffer = new StringBuffer(0);
                if ((msg.arg2 & BixolonPrinter.AUTO_STATUS_COVER_OPEN) == BixolonPrinter.AUTO_STATUS_COVER_OPEN) {
                    buffer.append("Cover is open.\n");
                }
                if ((msg.arg2 & BixolonPrinter.AUTO_STATUS_NO_PAPER) == BixolonPrinter.AUTO_STATUS_NO_PAPER) {
                    buffer.append("Paper end sensor: no paper present.\n");
                }

                if (buffer.capacity() > 0) {
                    Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "No error.", Toast.LENGTH_SHORT).show();
                }
                break;

            case BixolonPrinter.PROCESS_GET_NV_IMAGE_KEY_CODES:
                data = msg.getData();
                int[] value = data.getIntArray(BixolonPrinter.NV_IMAGE_KEY_CODES);

                Intent intent = new Intent();
                intent.setAction(ACTION_GET_DEFINEED_NV_IMAGE_KEY_CODES);
                intent.putExtra(EXTRA_NAME_NV_KEY_CODES, value);
                sendBroadcast(intent);
                break;

            case BixolonPrinter.PROCESS_EXECUTE_DIRECT_IO:
                buffer = new StringBuffer();
                data = msg.getData();
                byte[] response = data.getByteArray(BixolonPrinter.KEY_STRING_DIRECT_IO);
                for (int i = 0; i < response.length && response[i] != 0; i++) {
                    buffer.append(Integer.toHexString(response[i]) + " ");
                }

                Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_SHORT).show();
                break;

            case BixolonPrinter.PROCESS_MSR_TRACK:
                intent = new Intent();
                intent.setAction(ACTION_GET_MSR_TRACK_DATA);
                intent.putExtra(EXTRA_NAME_MSR_TRACK_DATA, msg.getData());
                sendBroadcast(intent);
                break;

            case BixolonPrinter.PROCESS_GET_MSR_MODE:

                break;
        }
    }


    private final Handler mHandler = new Handler(new Handler.Callback() {

        @SuppressWarnings("unchecked")
        @Override
        public boolean handleMessage(Message msg) {
            Log.d(TAG, "mHandler.handleMessage(" + msg + ")");

            switch (msg.what) {
                case BixolonPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BixolonPrinter.STATE_CONNECTED:

                            break;

                        case BixolonPrinter.STATE_CONNECTING:

                            break;
                        case BixolonPrinter.STATE_NONE:

                            break;
                    }
                    return true;

                case BixolonPrinter.MESSAGE_WRITE:
                    switch (msg.arg1) {
                        case BixolonPrinter.PROCESS_SET_DOUBLE_BYTE_FONT:
                            //mHandler.obtainMessage(MESSAGE_END_WORK).sendToTarget();

                            Toast.makeText(getApplicationContext(), "Complete to set double byte font.", Toast.LENGTH_SHORT).show();
                            break;

                        case BixolonPrinter.PROCESS_DEFINE_NV_IMAGE:
                            mBixolonPrinter.getDefinedNvImageKeyCodes();
                            Toast.makeText(getApplicationContext(), "Complete to define NV image", Toast.LENGTH_LONG).show();
                            break;

                        case BixolonPrinter.PROCESS_REMOVE_NV_IMAGE:
                            mBixolonPrinter.getDefinedNvImageKeyCodes();
                            Toast.makeText(getApplicationContext(), "Complete to remove NV image", Toast.LENGTH_LONG).show();
                            break;

                        case BixolonPrinter.PROCESS_UPDATE_FIRMWARE:
                            mBixolonPrinter.disconnect();
                            Toast.makeText(getApplicationContext(), "Complete to download firmware.\nPlease reboot the printer.", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    return true;

                case BixolonPrinter.MESSAGE_READ:
                    CheckInActivity.this.dispatchMessage(msg);
                    return true;

                case BixolonPrinter.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(BixolonPrinter.KEY_STRING_DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), mConnectedDeviceName, Toast.LENGTH_LONG).show();
                    return true;

                case BixolonPrinter.MESSAGE_TOAST:
                    //mListView.setEnabled(false);
                    Toast.makeText(getApplicationContext(), msg.getData().getString(BixolonPrinter.KEY_STRING_TOAST), Toast.LENGTH_SHORT).show();
                    return true;

                case BixolonPrinter.MESSAGE_BLUETOOTH_DEVICE_SET:
                    if (msg.obj == null) {
                        Toast.makeText(getApplicationContext(), "No paired device", Toast.LENGTH_SHORT).show();
                    } else {
                        DialogManager.showBluetoothDialog(CheckInActivity.this, (Set<BluetoothDevice>) msg.obj);
                    }
                    return true;

                case BixolonPrinter.MESSAGE_PRINT_COMPLETE:
                    Toast.makeText(getApplicationContext(), "Complete to print", Toast.LENGTH_SHORT).show();
                    return true;

                case BixolonPrinter.MESSAGE_ERROR_INVALID_ARGUMENT:
                    Toast.makeText(getApplicationContext(), "Invalid argument", Toast.LENGTH_SHORT).show();
                    return true;

                case BixolonPrinter.MESSAGE_ERROR_NV_MEMORY_CAPACITY:
                    Toast.makeText(getApplicationContext(), "NV memory capacity error", Toast.LENGTH_SHORT).show();
                    return true;

                case BixolonPrinter.MESSAGE_ERROR_OUT_OF_MEMORY:
                    Toast.makeText(getApplicationContext(), "Out of memory", Toast.LENGTH_SHORT).show();
                    return true;

                case BixolonPrinter.MESSAGE_COMPLETE_PROCESS_BITMAP:
                    String text = "Complete to process bitmap.";
                    Bundle data = msg.getData();
                    byte[] value = data.getByteArray(BixolonPrinter.KEY_STRING_MONO_PIXELS);
                    if (value != null) {
                        Intent intent = new Intent();
                        intent.setAction(ACTION_COMPLETE_PROCESS_BITMAP);
                        intent.putExtra(EXTRA_NAME_BITMAP_WIDTH, msg.arg1);
                        intent.putExtra(EXTRA_NAME_BITMAP_HEIGHT, msg.arg2);
                        intent.putExtra(EXTRA_NAME_BITMAP_PIXELS, value);
                        sendBroadcast(intent);
                    }

                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                    return true;

                case MESSAGE_START_WORK:

                    return true;

                case MESSAGE_END_WORK:

                    return true;

                case BixolonPrinter.MESSAGE_USB_DEVICE_SET:
                    if (msg.obj == null) {
                        Toast.makeText(getApplicationContext(), "No connected device", Toast.LENGTH_SHORT).show();
                    } else {

                    }
                    return true;

                case BixolonPrinter.MESSAGE_USB_SERIAL_SET:

                    return true;

                case BixolonPrinter.MESSAGE_NETWORK_DEVICE_SET:

                    return true;
            }
            return false;
        }
    });



    public void createDatabase()
    {
        DBHelper dbHelper = new DBHelper(this);
        try{
            dbHelper.createDataBase();
        }catch (IOException e)
        {
            Log.d("DATABASE IO", e.getMessage());
        }
        try{
            dbHelper.openDataBase();
        }catch (SQLException sqle)
        {
            Log.d("DATABASE SQL", sqle.getMessage());
        }
    }

    public int ingresarVehiculo(String identificacion, String matricula,int estado, String idEst,String fechaIngreso,String observacion)
    {
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int filas=-1;
        String sql="insert into automoviles(identificador,matricula,estado,idEstacionamiento,fechaIngreso,fechaSalida" +
                ",observacion) VALUES('"+identificacion+"','"+matricula+"',"+estado+",'"+idEst+
                "','"+fechaIngreso+"','','"+observacion+"')";
        Log.d(TAG,sql);
        try{
            db.execSQL(sql);
            filas=1;
            Log.d("SAVE", String.valueOf(filas));
        }catch (Exception e){
            filas=-2;
            Log.d("SAVE", e.getMessage());
        }
        db.close();
        return filas;

    }

}
