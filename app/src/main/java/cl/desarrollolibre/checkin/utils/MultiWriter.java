package cl.desarrollolibre.checkin.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.Map;

/**
 * Created by RafaelCastro on 12/5/15.
 */
public class MultiWriter

        implements Writer {

    @Override
    public BitMatrix encode(String contents,
                            BarcodeFormat format,
                            int width,
                            int height) throws WriterException {
        return encode(contents, format, width, height, null);
    }

    @Override
    public BitMatrix encode(String contents,
                            BarcodeFormat format,
                            int width, int height,
                            Map<EncodeHintType,?> hints) throws WriterException {

        Writer writer;
        writer = new Code128Writer();
        return writer.encode(contents, format, width, height, hints);
    }



    }

