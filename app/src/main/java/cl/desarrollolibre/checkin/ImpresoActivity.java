package cl.desarrollolibre.checkin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import cl.desarrollolibre.checkin.utils.Contents;
import cl.desarrollolibre.checkin.utils.QRCodeEncoder;

/**
 * Created by RafaelCastro on 12/9/15.
 */
public class ImpresoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    TextView lblCodigoBarras,lblIdentificador,lblIngreso;
    WindowManager manager;
    String codigoBarras,identificador,hora;
    RelativeLayout rlCheckIn,rlCheckOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impreso);
        Bundle extras = getIntent().getExtras();
        codigoBarras = extras.getString("placa");
        identificador = extras.getString("identificador");
        hora = extras.getString("hora");
        manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
        lblCodigoBarras = (TextView)findViewById(R.id.lblCodigoBarras);
        lblIdentificador = (TextView)findViewById(R.id.lblIdentificador);
        lblIngreso = (TextView)findViewById(R.id.lblIngreso);
        rlCheckIn = (RelativeLayout)findViewById(R.id.rlCheckIn);
        rlCheckOut = (RelativeLayout)findViewById(R.id.rlCheckOut);
        lblIdentificador.setText(identificador);
        lblIngreso.setText(hora);
        setSupportActionBar(toolbar);
        generaCodigoBarras(codigoBarras);

        rlCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImpresoActivity.this, CheckInActivity.class);
                intent.putExtra("tabPosition",0);
                startActivity(intent);
            }
        });

        rlCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImpresoActivity.this, CheckOutActivity.class);
                intent.putExtra("tabPosition", 1);
                startActivity(intent);
            }
        });
    }



    public void generaCodigoBarras(String barcode)
    {
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = 650;
        int height = 240;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension / 3;
        //Encode with a QR Code image

        if (barcode.equalsIgnoreCase("null")) {

        }

        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(barcode,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.CODE_128.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            ImageView myImage = (ImageView) findViewById(R.id.imgCodigoBarras);
            myImage.setVisibility(View.VISIBLE);
            lblCodigoBarras.setVisibility(View.VISIBLE);
            lblCodigoBarras.setText(barcode);
            myImage.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }
}
